import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './icons.json';
const Icons = createIconSetFromFontello(fontelloConfig, 'icons');
export default Icons;