import React, {Component} from 'react';
import { View, Text, TouchableHighlight, StyleSheet} from 'react-native';

export default class ButtonApp2 extends Component {
    render () {
        const { buttom1, action1, buttom2, action2  } = this.props;
        return (
            <View style={styles.down}>
                <TouchableHighlight
                    onPress={action1}
                    underlayColor='transparent'
                    style={{ paddingTop: 12 }}
                >
                    <Text style={styles.buttonDown}> {buttom1} </Text>
                </TouchableHighlight>

                <TouchableHighlight
                    onPress={action2}
                    underlayColor='transparent'
                    style={{ paddingTop: 12 }}
                >
                    <Text style={styles.buttonDown}> {buttom2} </Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    down: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1,
        marginBottom: 50,
        paddingBottom: 0,
        color: '#fff',

    },
    textDown: {
        color: '#fff',
        justifyContent: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    buttonDown: {
        fontFamily: 'NoirPro-Heavy',
        color: '#fff',
        fontSize: 21,
        padding: 10,
        borderColor: '#fff',
        borderWidth: 1,
        backgroundColor: '#9FBf42',
    }
})
