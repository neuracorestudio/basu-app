import React, {Component} from 'react';
import { View, Text, TouchableHighlight, StyleSheet} from 'react-native';

export default class ButtonApp extends Component {
    render () {
        const { buttom, text1, text2, action  } = this.props;
        return (
            <View style={styles.down}>
                <Text style={styles.textDown}>
                    {text1}
                    </Text>
                <Text style={styles.textDown}>
                    {text2}
                    </Text>
                <TouchableHighlight
                    onPress={action}
                    underlayColor='transparent'
                    style={{ paddingTop: 12 }}
                >
                    <Text style={styles.buttonDown}> {buttom} </Text>
                </TouchableHighlight>
            </View>
        
        );
    }
}

const styles = StyleSheet.create({
    
    down: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1,
        marginBottom: 50,
        paddingBottom: 0,
        color: '#fff',

    },
    textDown: {
        color: '#fff',
        justifyContent: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center',
        fontSize: 12,
    },
    buttonDown: {
        fontFamily: 'NoirPro-Heavy',
        color: '#fff',
        fontSize: 14,
        padding: 10,
        borderColor: '#fff',
        borderWidth: 1,
        backgroundColor: '#9FBf42',
    }
})
