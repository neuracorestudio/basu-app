import React, {Component} from 'react';
import { View, Text, StyleSheet, Image} from 'react-native';

export default class HeaderPaso extends Component {
    render () {
        const { num } = this.props;
        return (
            <View style={styles.header}>
                <View style={styles.headerText}>
                    <Text style={styles.text1Head}>Paso</Text>
                    <Text style={styles.text2Head}>No. {num} </Text>
                </View>
                <Image style={styles.logoBasu} source={require('../assets/basu_logo.png')} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    
    header: {
        justifyContent: 'flex-start',
        marginBottom: 2
    },
    headerText: {
        marginLeft: 20,
        marginTop: 15,
        flex: 1,
    },
    text1Head: {
        fontFamily: 'NoirPro-Regualr',
        fontSize: 14,
        color: '#94cc58',
        alignItems: 'flex-start'

    },
    text2Head: {
        fontFamily: 'NoirPro-Bold',
        fontSize: 20,
        color: '#94cc58'
    },
    logoBasu: {
        marginTop: 5,
        marginRight: 22,
        alignItems: 'flex-end',
        alignContent: 'flex-end',
        alignSelf: 'flex-end',
        width: 20,
        height: 40,
    },
})
