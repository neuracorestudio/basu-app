import React, { Component } from 'react';
import { ImageBackground } from 'react-native';


export default class Back2 extends Component {
    render() {

        const { children } = this.props;

        return (
            <ImageBackground
                source={require('../assets/city1.png')}
                style={{ flex: 1, width: '100%', backgroundColor: '#fff', marginBottom: -100 }}
            >
                {children}
            </ImageBackground>
        );
    }
}
