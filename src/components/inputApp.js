import React, { Component } from 'react';
import { StyleSheet, TextInput, KeyboardAvoidingView } from 'react-native';


export default class InputApp extends Component {

    constructor(props) {
        super(props);
        this.state = { textInput: '' };
    }

    render() {
        const { textDefault, type, max } = this.props;
            return (
                <KeyboardAvoidingView style={styles.container}>
                    <TextInput
                        style={styles.input}
                        onChangeText={(textInput) => this.setState({ textInput })}
                        value={this.state.textInput}
                        keyboardType={type}
                        maxLength={max}
                        placeholder={textDefault}
                        placeholderTextColor='#F9C534'
                    />
                </KeyboardAvoidingView>
            );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '80%'
    },
    input: {
        margin: 15,
        marginBottom: 5,
        marginTop: 10, 
        height: 45,
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        borderBottomWidth: 2,
        borderTopWidth: 0.3,
        fontFamily: 'NoirPro-SemiBold',
        color: '#F9C534',
        fontSize: 16,
        padding: 0,
        paddingLeft: 10,
        paddingRight: 10,
        textAlign: 'center',
        backgroundColor: '#fff'
    }
})
