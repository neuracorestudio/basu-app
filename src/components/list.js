import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, ImageBackground } from 'react-native';

const tips = [
    {
        key:1,
        name:'¿Sabías qué?',
        image: 'https://c.pxhere.com/photos/cc/3d/vintage_garage_sale_old_used_things_stuff_trash_items-681672.jpg!d',
        text: 'Un vaso de plástico tarda hasta 1,000 años en degradarse'
    },
    {
        key: 2,
        name: '¿Sabías qué?',
        image: 'https://basuimpact.com/images/basura.gif',
        text: 'Una botella de vidrio equivale a un foco encendido de 100 watts durante 4 horas'
    },
    {
        key: 3,
        name: '¿Sabías qué?',
        image: 'https://c.pxhere.com/photos/44/87/flow_mud_pool_chemistry_oil_pollution_gif-796741.jpg!d',
        text: 'Se necesitan 200 litros de agua aproximadamente para producir un solo litro de Coca-Cola'
    },
    {
        key: 4,
        name: '¿Sabías qué?',
        image: 'https://c.pxhere.com/photos/06/60/power_environment_pollution_air_smoke-1390844.jpg!d',
        text: 'Las personas que viven en lugares con altos niveles de contaminantes del aire tienen 20% más de riesgo de muerte por cáncer de pulmón'
    },
    {
        key: 5,
        name: '¿Sabías qué?',
        image: 'https://c.pxhere.com/photos/6a/b0/las_vegas_california_kalifornien_amerika_america_use_city-384673.jpg!d',
        text: 'El uso del aire acondicionado en tu auto incrementa el consumo de combustible hasta un 20%'
    },
]

export default class List extends Component {
    _renderItem(item){
        return(
            <ImageBackground style={styles.image} imageStyle={{ borderRadius: 15 }} source={{uri: item.image}}>
                <View style={styles.container}>
                    <Text style={styles.title}> {item.name} </Text>
                    <Text style={styles.text}> {item.text} </Text>
                </View>
            </ImageBackground>               
        )
    }
    render () {
        return (
            <View>
                <FlatList
                renderItem={({item}) => this._renderItem(item)}
                data={tips}
                horizontal
                ItemSeparatorComponent={() => <View style={{width: 10}}/>}
                showsHorizontalScrollIndicator={false}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    container: {
        backgroundColor: 'rgba(0,0,0,.5)',
        width: '100%',
        height: '100%',
        borderRadius: 15,
        justifyContent: 'center'
    },
    image:{
        width: 260, 
        height: 130,
        justifyContent: 'center', 
        marginTop: 20,
        
    },
    title:{
        color: '#fff',
        textAlign: "center",
        fontSize: 18,
        fontFamily: 'NoirPro-Bold',
        marginBottom: 2
    },
    text:{
        color: '#fff',
        textAlign: "center",
        fontSize: 12,
        fontFamily: 'NoirPro-SemiBold'
    },
})