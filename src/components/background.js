import React, { Component } from 'react';
import { ImageBackground } from 'react-native';

export default class Back extends Component {
    render() {
        const { children } = this.props;
        return (
            <ImageBackground
                source={require('../assets/city2.png')}
                style={{ flex: 1, width: '100%', backgroundColor: '#fff' }}
            >
                {children}
            </ImageBackground>

        );
    }
}
