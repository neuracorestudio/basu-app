import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

export default class HeaderCenter extends Component {
    render() {
        const { title, header1, header2, text1, text2, text3} = this.props;
        return(
            <View style={styles.head}>
                <Image style={styles.logoBasu} source={require('../assets/basu_logo.png')} />
                <Text style={styles.text2}>{title}</Text>
                <Text style={styles.textHeader}>{header1}</Text>
                <Text style={styles.textHeader}>{header2}</Text>
                <View style={styles.containerText}>
                    <Text style={styles.text}>{text1}</Text>
                    <Text style={styles.text}>{text2}</Text>
                    <Text style={styles.text}>{text3}</Text>
                </View>
            </View>
         );
    }
}

const styles = StyleSheet.create({
    head: {
        alignItems: 'center'
    },
    logoBasu: {
        marginTop: 20,
        marginBottom: 15,

        width: 20,
        height: 40,
    },
    textHeader: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#94cc58',
        fontSize: 20,
    },
    containerText: {
        marginTop: 10,
        marginBottom: 12,
        alignItems: 'center'
    },
    text: {
        color: '#94cc58',
        fontFamily: 'NoirPro-Regular',
        fontSize: 12,
    },
    text2: {
        fontFamily: 'NoirPro-Regular',
        color: '#94cc58',
        fontSize: 17,
    },
})
