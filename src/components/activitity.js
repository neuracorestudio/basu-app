import React, { Component } from 'react'
import { ActivityIndicator, StyleSheet, Text, View} from 'react-native'

export default class Activity extends Component {
    render() {
        return (
            <View style={styles.container}>
            <Text style={{color: '#fff', fontSize: 16 }}>Procesado imagen</Text>
                <ActivityIndicator size={150} color="#89C53f" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,.4)',
        position: 'absolute',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
})
