import React, { Component } from 'react';
import { StyleSheet, TextInput, KeyboardAvoidingView } from 'react-native';

export default class InputLogin extends Component {

    constructor(props) {
        super(props);
        this.state = { textInput: '' };
    }

    render() {
        const { textDefault, type, max, pass } = this.props;
            return (
                <KeyboardAvoidingView style={styles.container}>
                    <TextInput
                        style={styles.input}
                        onChangeText={(textInput) => this.setState({ textInput })}
                        value={this.state.textInput}
                        keyboardType={type}
                        maxLength={max}
                        placeholder={textDefault}
                        placeholderTextColor='#F9C534'
                        secureTextEntry={pass}
                    />
                </KeyboardAvoidingView>
            );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '90%',
    },
    input: {
        borderRadius: 12,
        margin: 10,
        height: 48,
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        fontFamily: 'NoirPro-SemiBold',
        color: '#F9C534',
        fontSize: 21,
        borderBottomColor: 'gray',
        borderBottomWidth: 2,
        borderTopWidth: 0.3,
        padding: 0,
        backgroundColor: '#fff',
        textAlign: 'center',
    }
})
