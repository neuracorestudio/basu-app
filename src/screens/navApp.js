import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';

import Home from "./app/home";
import Clabe from "./app/clabe";
import Top from "./app/top";
import Icons from '../config/icons'

const NavApp = createBottomTabNavigator(
    {
        
        Home: { 
            screen: Home,
            title: 'Inicio',
            navigationOptions: {
                title: '',
                tabBarIcon: ({ focused }) => (<Icons name={'home'} size={22}
                    focused={focused}
                    color={focused ? '#F9c534' : '#fff'} />),
            },
         },
        Top: { 
            screen: Top,
            title: 'Top',
            navigationOptions: {
                title: '',
                tabBarIcon: ({ focused }) => ( <Icons name={'top'} size={22}
                    focused={focused}
                    color={focused ? '#F9c534' : '#fff'} />),
            },
        },
        Clabe: { 
            screen: Clabe,
            title: 'Clabe',
            navigationOptions: {
                title: '',
                    tabBarIcon: ({ focused }) => (<Icons name={'clabe'} size={22}
                    focused={focused}
                    color={focused ? '#F9c534' : '#fff'} />),
            },
        },
    },
    {
        tabBarOptions: {
            activeTintColor: '#F9C534',
            activeBackgroundColor: 'rgba(0,0,0,0)',
            style: {
                backgroundColor: 'rgba(0,0,0,0)',
                marginTop: 0,
                position: 'absolute',
                left: 30,
                right: 30,
                bottom: -5,
                borderTopWidth: 0,
            }
        },
        tabStyle: {
            backgroundColor: '#fff',
        }
    }


);

export default NavApp;