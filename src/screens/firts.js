import { createStackNavigator } from 'react-navigation';

import Inicio from "./login/inicio";
import Registro from "./login/registro";
import NavApp from "./navApp";
import Form from "./login/form";

 const Start = createStackNavigator(
    {
        Inicio,
        Registro,
        Form,
        NavApp
    },
    {
        headerMode: 'none',
    }
        
)

export default Start;