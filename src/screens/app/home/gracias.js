import React, { Component } from 'react';
import { Text, StyleSheet, Image, KeyboardAvoidingView } from 'react-native';

import Back from '../../../components/background';
import ButtonApp from '../../../components/buttonApp';
import HeaderPaso from '../../../components/headerPaso';

export default class Gracias extends Component {
    render() {   
        return (
            <Back>
                <HeaderPaso num='4' />
                <KeyboardAvoidingView style={styles.head}>       
                    <Image style={styles.image} source={require('../../../assets/eco_world.png')} />
                    <Text style={styles.textHeader}>GRACIAS</Text>
                    <Text style={styles.textHeader}>POR RECICLAR</Text>
                    
                    <Text style={styles.text1Head}>Has ganado</Text>
                    <Text style={styles.text2Head}>10 pts</Text>

                    <Text style={styles.text}>Tu empaque ayuda a ahorrar</Text>
                    <Text style={styles.text}>104 gramos de CO2 que pueden </Text>
                    <Text style={styles.text}>contaminar el planeta</Text>
                </KeyboardAvoidingView>

                <ButtonApp
                    buttom='FINALIZAR'
                    action={() => this.props.navigation.navigate('InicioHome')}
                />
            </Back>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        justifyContent: 'flex-start',
    },
    headerText: {
        marginLeft: 20,
        marginTop: 15,
        flex: 1,
    },
    text1Head: {
        fontFamily: 'NoirPro-SemiBold',
        fontSize: 18,
        color: '#94cc58',
        alignItems: 'flex-start'

    },
    text2Head: {
        fontFamily: 'NoirPro-SemiBold',
        fontSize: 24,
        color: '#F9C534',
        marginBottom: 8,
    },
    head: {
        alignItems: 'center'
    },
    textHeader: {
        fontFamily: 'NoirPro-Bold',
        color: '#F9C534',
        fontSize: 20,
    },
    text: {
        color: '#94cc58',
        fontFamily: 'NoirPro-Regular',
        fontSize: 12,
    },
    image: {
        margin: 12,
    },
})
