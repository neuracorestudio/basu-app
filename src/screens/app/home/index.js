import { createStackNavigator } from 'react-navigation';

import InicioHome from "./inicio";
import Reciclar from "./reciclar";
import Colocar from './colocar';
import Identificar from './identificar';
import Gracias from './gracias';

const Home = createStackNavigator(
    {
        InicioHome,
        Reciclar,
        Colocar,
        Identificar,
        Gracias,
    },
    {
        headerMode: 'none',
    }
)

export default Home;