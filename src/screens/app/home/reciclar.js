import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableHighlight, Alert } from 'react-native';

import Back from '../../../components/background';
import HeaderPaso from '../../../components/headerPaso';
import ButtonApp from '../../../components/buttonApp';

export default class Reciclar extends Component {
    render() {
        const Pet = () => {
            Alert.alert('',
                'Podrás reciclar botellas de bebidas de este material con una capacidad mínima de 500 ml y máxima de 1.5 litros. Asegúrate de quitarle la tapa y depositarla en el compartimento correspondiente.', 
                [
                    { text: 'OK', onPress: () => console.log('info puntos') },
                ],
                { cancelable: false })
        }

        const Aluminio = () => {
            Alert.alert('',
                'Las latas de comida que recicles, por higiene tienen que estar lavadas previamente. Aceptamos la capacidad mínima de 50gr y máxima de 400gr. De bebidas la capacidad mínima de 150 ml y máxima de 1 litro.',
                [
                    { text: 'OK', onPress: () => console.log('info co2') },
                ],
                { cancelable: false })
        }

        const Vidrio = () => {
            Alert.alert('',
                'Podrás reciclar únicamente recipientes de líquidos de este material con una capacidad mínima de 250 ml y máxima de 1 litro.',
                [
                    { text: 'OK', onPress: () => console.log('info co2') },
                ],
                { cancelable: false })
        }

        return (
            <Back>
                <HeaderPaso num='1' />

                <View style={styles.header}>
                    <Text style={styles.textHeader}>REVISA</Text>
                    <Text style={styles.textHeader}>TU PRODUCTO</Text>

                    <Text style={styles.text}>1. Tu porducto debe estar vacio</Text>
                    <Text style={styles.text}>y sin residuos</Text>
                    <Text style={styles.text2}>2. Solo podras reciclar:</Text>
                </View>

                <View style={styles.score}>
                    <TouchableHighlight onPress={Pet} underlayColor='rgba(0,0,0,0)'>
                        <View style={styles.scoreItem}>
                            <Image source={require('../../../assets/pet.png')} style={{ justifyContent: 'center' }} />
                        </View>
                    </TouchableHighlight>

                    <TouchableHighlight onPress={Aluminio} underlayColor='rgba(0,0,0,0)'>
                        <View style={styles.scoreItem}>
                            <Image source={require('../../../assets/aluminio.png')} style={{ justifyContent: 'center' }} />
                        </View>
                    </TouchableHighlight>

                    <TouchableHighlight onPress={Vidrio} underlayColor='rgba(0,0,0,0)'>
                        <View style={styles.scoreItem}>
                            <Image source={require('../../../assets/vidrio.png')} style={{ justifyContent: 'center' }} />
                        </View>
                    </TouchableHighlight>
                </View>

                <ButtonApp
                    buttom='CONTINUAR'
                    action={() => this.props.navigation.navigate('Colocar')}
                />
            </Back>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        alignItems: 'center'
    },
    score: {
        flex: 1.2,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
    },
    scoreItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 12,
        paddingRight: 12,
    },
    scoreItemText: {
        color: '#89C53F',
        fontFamily: 'NoirPro-Regular',
        fontSize: 21,
    },
    scoreItemNum: {
        fontFamily: 'NoirPro-Bold',
        fontSize: 20,
        color: '#F9C534',
    },
    scoreItemUni: {
        fontSize: 14,
    },
    textHeader: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#F9C534',
        fontSize: 24,
    },
    text: {
        color: '#94cc58',
        fontFamily: 'NoirPro-Regular',
    },
    text2: {
        fontFamily: 'NoirPro-Regular',
        color: '#94cc58',
        margin: 10,
    },
})