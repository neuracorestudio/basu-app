import React, { Component } from 'react';
import { View, StyleSheet, Modal, PermissionsAndroid, Alert } from 'react-native';
import { CameraKitCameraScreen } from 'react-native-camera-kit';

import Back from '../../../components/background';
import ButtonApp from '../../../components/buttonApp';
import HeaderPaso from '../../../components/headerPaso';
import Activity from '../../../components/activitity';

export default class Identificar extends Component {
    _renderItem() {
        return (
            <Activity/>
        )
    }

    state = { modalVisible: false, isPermitted: false }

    static navigationOptions = {
        header: null,
    };

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    constructor(props) {
        super(props);
        var that = this;
        async function requestCameraPermission() {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'Permiso a cámara denegado :(',
                        'message': 'BASU necesita acceder a tu cámara para poder reciclar'
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    that.setState({ isPermitted: true })
                } else {
                    alert("CAMARA: permiso denegado")
                }
            } catch (err) {
                alert("Error al solicitar permisos de la cámara", err);
                console.warn(err)
            }
        }

        requestCameraPermission();
    }

    onBottomButtonPressed(event) {
        const captureImages = JSON.stringify(event.captureImages);
        Alert.alert(
            event.type,
            captureImages,
            [{ text: 'OKakdl', onPress: () => this.props.navigation.navigate('Gracias') }],
            { cancelable: false }
        );
    }
    
    render() {
        if (this.state.isPermitted) {
            return (
                <View style={styles.container}>
                    <Modal
                        animationType="none"
                        visible={this.state.modalVisible}
                        transparent={true}
                    >
                        <Activity  />                        
                    </Modal>
                    <HeaderPaso style= {{paddingBottom: 20}} num='3' />
                    <CameraKitCameraScreen
                        style={styles.camara}
                        scanBarcode={true}
                        laserColor={"blue"}
                        frameColor={"yellow"}
                        heightForScannerFrame={300}
                        colorForScannerFrame={'red'}
                        />
                    <ButtonApp
                        text1='Ubica tu producto en el centro'
                        text2='para poder reconocerlo'
                        buttom='CONTINUAR'
                        action={() => {
                            this.setModalVisible(true);
                            setTimeout(() => this.setModalVisible(false), 2500);
                            setTimeout(() => this.props.navigation.navigate('Gracias'), 2501);
                        }}
                    />
                </View>
            );
        } else {
        return (
            <Back>
                <HeaderPaso num='3' />
                
                <ButtonApp
                    buttom='Regresar'
                    action={() => this.props.navigation.navigate('InicioHome')}
                />
            </Back>
        );
        }
    }
}

const styles = StyleSheet.create({
    header: {
        justifyContent: 'flex-start',
    },
    camara: {
        flex: 1,
        height: '100%',
        width: '100%',
        marginBottom: 0,
        paddingBottom: 0,
    },
    container: {
        flex: 1,
    }
})
