import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableHighlight, Alert } from 'react-native';

import Back from '../../../components/background';
import List from '../../../components/list';
import ButtonApp from '../../../components/buttonApp';
import HeaderBasu from '../../../components/header';

export default class InicioHome extends Component {

    constructor(props) {
        super(props)
        this.state = { pts: 0, grs: 0 }
    }

    subirScore = () => {
        this.setState({
            pts: this.state.pts + 10,
            grs: this.state.grs + 104,
        })
    }

    render() {
        const AlertPts = () => {
            Alert.alert('',
                'Tus puntos acumulados serán monetizados y trasferidos a la cuenta que configuraste al alcanzar los 1000 puntos',
                [
                    { text: 'OK', onPress: () => console.log('info puntos') },
                ],
                { cancelable: false })
        }

        const AlertReciclar = () => {
            Alert.alert('',
                'Al reciclar ayudas a reducir las emisiones de CO2. A esto se le conoce como HUELLA DE CARBONO',
                [
                    { text: 'OK', onPress: () => console.log('info co2') },
                ],
                { cancelable: false })
        }

        return (                
            <Back>
                <HeaderBasu
                    header='Hola'
                    title='Bienvenido'
                />

                <View style={styles.carrucel}>
                    <List/>
                 </View>

                <View style={styles.score}>   
                    <TouchableHighlight onPress={AlertPts} underlayColor='rgba(0,0,0,0)'>
                        <View style={styles.scoreItem}>
                            <Text style={styles.scoreItemText}>Tienes</Text>
                            <Text style={styles.scoreItemText}>acumulados</Text>
                                <Text style={styles.scoreItemNum}>
                                    {this.state.pts}<Text style={styles.scoreItemUni}> pts</Text>
                                </Text>
                            <Image source={require('../../../assets/info.png')} style={{ justifyContent: 'center' }} />
                        </View>
                    </TouchableHighlight>

                    <TouchableHighlight onPress={AlertReciclar} underlayColor='rgba(0,0,0,0)'>
                        <View style={styles.scoreItem}>
                            <Text style={styles.scoreItemText}>Ahorro en tu</Text>
                            <Text style={styles.scoreItemText}>huella de carbono</Text>
                                <Text style={styles.scoreItemNum}>
                                    {this.state.grs}<Text style={styles.scoreItemUni}> g</Text>
                                </Text>
                            <Image source={require('../../../assets/info.png')} style={{ justifyContent: 'center' }} />
                        </View>
                    </TouchableHighlight>
                </View>

                <View style={styles.down}></View>
                
                <ButtonApp style={{ flex: 3,}}
                    text1='Gana puntos reciclando y ayudando a reducir las'
                    text2='emiciones de CO2 para salvar el planeta'
                    buttom='RECICLAR'
                    action={() => {
                        setTimeout(() => this.subirScore(), 1000);
                        this.props.navigation.navigate('Reciclar');
                    }}
                />
            </Back>
        );
    }
}

const styles = StyleSheet.create({
    down:{
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 2,
        marginBottom: 60,
        paddingBottom: 0,
        color: '#fff',
    },
    score:{
        flex: 1.2,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        paddingTop: 15,
    },
    scoreItem:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
    },
    scoreItemText:{
        color: '#94cc58',
        fontFamily: 'NoirPro-Regular',
        fontSize: 12,
    },
    scoreItemNum:{
        fontFamily: 'NoirPro-Bold',
        fontSize: 24,
        color: '#F9C534',
    },
    scoreItemUni:{
        fontSize: 18,
    },
    carrucel:{
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
})