import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, } from 'react-native';

import Back from '../../../components/background';
import ButtonApp from '../../../components/buttonApp'
import HeaderPaso from '../../../components/headerPaso';

export default class Colocar extends Component {
    render() {
        return (
            <Back>
                <HeaderPaso num='2'/>
                <View style={styles.head}>
                    <Text style={styles.textHeader}>COLOCA</Text>
                    <Text style={styles.textHeader}>TU PRODUCTO</Text>

                    <Text style={styles.text}>En el bote correspondiente al</Text>
                    <Text style={styles.text}>material, cuida que tu empaque este</Text>
                    <Text style={styles.text}>colocado en el centro del rectángulo</Text>
                    <Text style={styles.text}>amarillo para continuar.</Text>
                    <View>    
                         <View style={styles.containerImages}>
                         <Image style={styles.image} source={require('../../../assets/coloca.png')} />
                         </View>
                    </View>
                </View>

                <ButtonApp
                    buttom='CONTINUAR'
                    action={() => this.props.navigation.navigate('Identificar')}
                />
            </Back>
        );
    }
}

const styles = StyleSheet.create({
    head: {
        alignItems: 'center'
    },
    textHeader: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#F9C534',
        fontSize: 20,
    },
    text: {
        color: '#94cc58',
        fontFamily: 'NoirPro-Regular',
        fontSize: 12,
    },
    containerImages: {
        alignContent: "center",
        flexDirection: 'row',
        display: 'flex',
    },
    image: {
        margin: 15,
        height: 170,
        width: 160,
    },
})
