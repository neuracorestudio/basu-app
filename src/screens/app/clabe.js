import React, { Component } from 'react';
import { View, Alert, StyleSheet, } from 'react-native';

import Back from '../../components/background';
import ButtonApp from '../../components/buttonApp';
import InputApp from '../../components/inputApp';
import HeaderCenter from '../../components/headerCenter';

export default class Clabe extends Component {    
    render() {
        const RegistroClabe = () => {
            Alert.alert('GRACIAS  :)',
                'Tu cuenta fue dada de alta exitosamente, por tu seguridad cada vez que juntes 1,000 puntos se trasferirá el equivalente en pesos mexicanos a esta cuenta',
                [
                    { text: 'OK', onPress: () => console.log('CLABE') },
                ],
                { cancelable: false })
        }
        return (
            <Back>
                <HeaderCenter
                    title='CONFIGURA TU'
                    header1='CUENTA DE RETIRO'
                    text1='Esta cuenta servirá para'
                    text2='trasnferir tus puntos y'
                    text3='convertirlos en dinero.'
                />
                <View style={styles.head}>
                <InputApp
                    textDefault='CUENTA CLABE'
                    max={18}
                    type='numeric'
                    />
                    
                <InputApp
                    typw='default'
                    textDefault='TITULAR'
                />
                        
            </View>
                <ButtonApp 
                    text1='Por tu seguridad no podemos reliazar niguna' 
                    text2='transferencia de fondos de forma manual' 
                    buttom='ACEPTAR'
                    action={RegistroClabe}
                />
            </Back>
        );
    }
}

const styles = StyleSheet.create({
    head:{
        alignItems: 'center'
    }
})
