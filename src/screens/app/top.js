import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Back2 from '../../components/background2';
import HeaderCenter from '../../components/headerCenter'

export default class Clabe extends Component {
    render() {
        return (
            <Back2>
                <HeaderCenter 
                    title='BASU TOP 5'
                    header1='AMIGOS DEL MEDIO'
                    header2='AMBIENTE'
                    text1='Aquí te mostramos a los usuarios'
                    text2='que mas han reducido su huella'
                    text3='de carbono a través de BASU.'
                />
                <View style={styles.head}>
                    <View style={styles.winner}>
                        <Text style={styles.win1}>1 </Text>
                        <Text style={styles.win1}> NOMBRE APELLIDO </Text>
                        <Text style={styles.win1}> 10 kg</Text>
                    </View>
                    <View style={styles.winner}>
                        <Text style={styles.win2}>2 </Text>
                        <Text style={styles.win2}> NOMBRE APELLIDO </Text>
                        <Text style={styles.win2}> 8 kg</Text>
                    </View>
                    <View style={styles.winner}>
                        <Text style={styles.win3}>3 </Text>
                        <Text style={styles.win3}> NOMBRE APELLIDO </Text>
                        <Text style={styles.win3}> 3 kg</Text>
                    </View>
                    <View style={styles.winner}>
                        <Text style={styles.win4}>4 </Text>
                        <Text style={styles.win4}> NOMBRE APELLIDO </Text>
                        <Text style={styles.win4}> 0.8 kg</Text>
                    </View>
                    <View style={styles.winner}>
                        <Text style={styles.win5}>5 </Text>
                        <Text style={styles.win5}> NOMBRE APELLIDO </Text>
                        <Text style={styles.win5}> 0.5 kg</Text>
                    </View>
                </View>
            </Back2>
        );
    }
}

const styles = StyleSheet.create({
    head: {
        alignItems: 'center'
    },
    winner:{
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
    },
    win1:{
        fontFamily: 'NoirPro-SemiBold',
        color: '#F9C534',
        fontSize: 16,
        marginBottom: 10,
    },
    win2: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#ccc',
        fontSize: 16,
        marginBottom: 10,
    },
    win3: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#cda434',
        fontSize: 16,
        marginBottom: 10,
    },
    win4: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#94cc58',
        fontSize: 16,
        marginBottom: 10,
    },
    win5: {
        fontFamily: 'NoirPro-SemiBold',
        color: '#94cc58',
        fontSize: 16,
        marginBottom: 10,
    }
})
