import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, TouchableOpacity } from 'react-native';

import InputLogin from '../../components/inputLogin';

export default class Inicio extends Component {
    render() {
        const AlertCorreo = () => {
            Alert.alert('BASU aún está en desarrollo',
                'Pero te puedes pre registrar en nuestro formulario y estar al tanto para que te avisemos cuando ya este listo :)',
                [
                    { text: 'Ir', onPress: () => this.props.navigation.navigate('Form') },
                    { text: 'Cancelar', onPress: () => console.log('ok') },
                ],
                { cancelable: false })
        }
        return (

            <View style={styles.container}>
            
                <Image
                    source={require('../../assets/white_logo_basu.jpg')}
                    style={{ width: 40, height: 64 }}
                />
                <InputLogin
                    textDefault='USUARIO'
                    type='default'
                    />

                <InputLogin
                    textDefault='E-MAIL'
                    type='email-address'
                />

                <InputLogin
                    textDefault='CONTRASEÑA'
                    type='default'
                    pass={true}
                />

                <InputLogin
                    textDefault='REPETIR CONTRASEÑA'
                    type='default'
                    pass={true}
                    />

                <TouchableOpacity style={styles.button} onPress={AlertCorreo}>
                    <Text style={styles.textButton}>PRE REGISTRO</Text>
                </TouchableOpacity>

                <Image
                    source={require('../../assets/fb.png')}
                    style={{ width: 45, height: 45, margin: 10 }}
                />

                <TouchableOpacity onPress={() => this.props.navigation.navigate('Registro')}>
                <Text style={styles.text}>
                    ¿Ya tienes una cuenta? Inicia sesión <Text style={{ textDecorationLine:'underline' }}>aquí</Text>
                </Text>
                </TouchableOpacity>

                <Text style={styles.text}>{"\n"}Al registrate aceptas nuestros</Text>
                <Text style={styles.text}>Terminos y condiciones</Text>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#89C53F',
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    textButton: {
        color: 'white',
        fontSize: 22,
        fontWeight: 'bold',
        fontFamily: 'NoirPro-Bold',
    },
    button: {
        backgroundColor: '#F9C534',
        marginTop: 20,
        width: '90%',
        height: 48,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderTopWidth: 0.3,
        borderWidth: 1,
        borderColor: 'gray',
    },
    text: {
        color: '#fff'
    }
})
