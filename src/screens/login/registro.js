import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

import InputLogin from '../../components/inputLogin'


export default class Registro extends Component {
    render() {        
        return (
            <View style={styles.container}>

                <Image
                    source={require('../../assets/white_logo_basu.jpg')}
                    style={{ width: 40, height: 62, marginBottom: 25 }}
                />

                <InputLogin
                    textDefault='USUARIO'
                    type='default'
                    />

                <InputLogin
                    textDefault='CONTRASEÑA'
                    type='default'
                    pass={true}
                />

                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('NavApp')}>
                    <Text style={styles.textButton}>INICIAR SESIÓN</Text>
                </TouchableOpacity>

                <Image
                    source={require('../../assets/fb.png')}
                    style={{ width: 45, height: 45, margin: 12 }}
                />

                <TouchableOpacity onPress={() => this.props.navigation.navigate('Inicio')}>
                    <Text style={styles.text}>
                        ¿No tienes una cuenta? Pre registrate <Text style={{ textDecorationLine: 'underline' }}>aquí</Text>
                    </Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //paddingTop:20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#89C53F',
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    input: {
        height: 40,
        width: 180,
        borderColor: '#ccc',
        borderWidth: 2,
        alignItems: 'center'
    }, 
    textButton: {
        color: 'white',
        fontSize: 22,
        fontWeight: 'bold',
        fontFamily: 'NoirPro-Bold',
    },
    button: {
        backgroundColor: '#F9C534',
        marginTop: 20,
        width: '90%',
        height: 48,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderTopWidth: 0.3,
        borderWidth: 1,
        borderColor: 'gray',
    },
    text: {
        color: '#fff'
    }
});