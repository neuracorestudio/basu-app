import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, WebView, TouchableOpacity } from 'react-native';

export default class Form extends Component {
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.headerText} onPress={()=> this.props.navigation.navigate('Inicio')} >
                    <Text style={styles.text1Head}>Regresar</Text>
                </TouchableOpacity>
                    <Image style={styles.logoBasu} source={require('../../assets/white_logo_basu.jpg')} />
                <TouchableOpacity style={styles.headerText2} onPress={()=> this.props.navigation.navigate('NavApp')} >
                    <Text style={styles.text1Head}>Ver demo</Text>
                </TouchableOpacity>
            </View>
                <WebView
                    source={{ uri: 'https://goo.gl/forms/T9dXhcyZWlWbtN4V2'}}
                    style={styles.web}
                /> 
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    header: {
        paddingBottom: 10,
        backgroundColor: '#94cc58',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    headerText: {
        flex: 1,
        marginLeft: 20,
        marginTop: 20,
    },
    headerText2: {
        flex: 1,
        marginRight: 20,
        marginTop: 20,
        alignItems: 'flex-end',
        
    },
    text1Head: {
        fontSize: 19,
        color: '#fff',
    },
    text2Head: {
        fontSize: 29,
        color: '#fff'
    },
    logoBasu: {
        marginTop: 5,
        width: 27,
        height: 53,
    },
    web: {
        flex: 3,
    }
})
